# PBJS

### Installation

PBJS requires [Node.js](https://nodejs.org/) v8+ to run and to build.

Install the dependencies and devDependencies and start the server.

```sh
$ git clone https://gitlab.com/dpv101/exmarketplace-pbjs.git
$ cd exmarketplace-pbjs
$ yarn install
$ yarn start
```

### Plugins

Dillinger is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

| Plugin | README |
| ------ | ------ |
| prebid.js | https://github.com/prebid/Prebid.js/blob/master/README.md
| ArcAds | https://github.com/washingtonpost/ArcAds/blob/master/README.md

#### Building for source
For production release or final ads to website:

```sh
$ yarn build
```

and copy the file generated inside of dist folder to any server and import the script over the web site that will content the ads configured.

### Init Ads service
```js
    import PbjsUtils from '../pbjs_utils.js';
    let pbjsInstance = new PbjsUtils();
```

If you need set price granularity: 
```js
    pbjsInstance.setPriceGranularity('low');
```
View more documentation in: http://prebid.org/dev-docs/publisher-api-reference.html

If you want connect with prebid server:
```js
    pbjsInstance.setS2sConfig({
        accountId: '1',
        enabled: true,
        defaultVendor: 'appnexus',
        bidders: ['appnexus'],
        publisherDomain: "http://businesscasestudies.co.uk", // Used for SafeFrame creative.
        timeout: 1500,
        adapter: 'prebidServer',
        endpoint: 'https://hbserver.exmarketplace.com/openrtb2/auction',
        syncEndpoint: 'https://hbserver.exmarketplace.com/cookie_sync',
    });
```
View more documentation in: http://prebid.org/dev-docs/publisher-api-reference.html

### Init ArcAds
```js
    pbjsInstance.initAsceAds({
        dfp: {
            id: '42150330'
        },
        bidding: {
            prebid: {
            enabled: true,
            timeout: 2000,
            sizeConfig: [
                {
                'mediaQuery': '(min-width: 1024px)',
                'sizesSupported': [[970, 250], [970, 90], [728, 90], [468, 60], [300, 600], [300, 250], [160, 600], [120, 600]],
                'labels': ['desktop']
                }, 
                {
                'mediaQuery': '(min-width: 480px) and (max-width: 1023px)',
                'sizesSupported': [[728, 90], [468, 60], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 250]],
                'labels': ['tablet']
                }, 
                {
                'mediaQuery': '(min-width: 0px)',
                'sizesSupported': [[360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 250]],
                'labels': ['phone']
                }
            ]
            }
        }
    });
```

View more documentation in: https://github.com/washingtonpost/ArcAds

### HTML structure example from file index.html
    
        <!DOCTYPE html>
        <html lang="en">
        <head>
        <title>A JavaScript project</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- IMPORTANT SCRIPTS -->
        <link href="https://vjs.zencdn.net/7.3.0/video-js.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/videojs-vast-vpaid/2.0.2/videojs.vast.vpaid.min.css" rel="stylesheet">

        <script async src="https://static.quantcast.mgr.consensu.org/v34/cmp.js" integrity="sha256-wcK00ailZ7l1Ms9uqdH9tPXyZOiUXgWY28cCd+HzMM4= sha256-rppMsoAp/s+PmlDSPRncURhvFIgHaFKMhUGAdhXTQyc= sha256-qg1LZBCXUrTUo2Dr0qGpsmdjZN6AaxPnYQrcbRn7ghk= sha256-ot3T4uPdxNNqyiVJ8a35qyxwRRvDQGHFcwtbIkAAbUo=" crossorigin="anonymous" type="text/javascript"></script>
        <script async src="/ad_contents_example/ad_example.js" type="text/javascript"></script>
        </head>
        <body>
        <div style="display: flex;">
            <div style="width: 60%;text-align: justify;padding: 30px;" class="td-page-content">
            <h2>Basic Prebid.js Example</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo mauris, hendrerit at felis in, feugiat blandit tortor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse posuere, lectus et pulvinar facilisis, felis mi faucibus purus, vel tempus purus dolor et enim. Aliquam sollicitudin velit ac enim pretium fermentum. Duis ac metus nec mauris varius volutpat vel sed lorem. Curabitur malesuada vehicula lorem at convallis. Nulla at augue dui. Vestibulum risus metus, scelerisque ut felis vel, sodales pretium libero. Proin efficitur lorem vitae diam facilisis vestibulum. Nam molestie suscipit nibh ac porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas bibendum, libero vel volutpat tincidunt, lacus lorem porttitor nisi, eu mollis turpis est vitae enim. Suspendisse potenti. Integer lacinia neque sit amet lorem rutrum, ac commodo mi porta. Mauris commodo a odio sit amet eleifend. Curabitur nunc orci, elementum at lectus vitae, egestas rutrum ante.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo mauris, hendrerit at felis in, feugiat blandit tortor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse posuere, lectus et pulvinar facilisis, felis mi faucibus purus, vel tempus purus dolor et enim. Aliquam sollicitudin velit ac enim pretium fermentum. Duis ac metus nec mauris varius volutpat vel sed lorem. Curabitur malesuada vehicula lorem at convallis. Nulla at augue dui. Vestibulum risus metus, scelerisque ut felis vel, sodales pretium libero. Proin efficitur lorem vitae diam facilisis vestibulum. Nam molestie suscipit nibh ac porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas bibendum, libero vel volutpat tincidunt, lacus lorem porttitor nisi, eu mollis turpis est vitae enim. Suspendisse potenti. Integer lacinia neque sit amet lorem rutrum, ac commodo mi porta. Mauris commodo a odio sit amet eleifend. Curabitur nunc orci, elementum at lectus vitae, egestas rutrum ante.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo mauris, hendrerit at felis in, feugiat blandit tortor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse posuere, lectus et pulvinar facilisis, felis mi faucibus purus, vel tempus purus dolor et enim. Aliquam sollicitudin velit ac enim pretium fermentum. Duis ac metus nec mauris varius volutpat vel sed lorem. Curabitur malesuada vehicula lorem at convallis. Nulla at augue dui. Vestibulum risus metus, scelerisque ut felis vel, sodales pretium libero. Proin efficitur lorem vitae diam facilisis vestibulum. Nam molestie suscipit nibh ac porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas bibendum, libero vel volutpat tincidunt, lacus lorem porttitor nisi, eu mollis turpis est vitae enim. Suspendisse potenti. Integer lacinia neque sit amet lorem rutrum, ac commodo mi porta. Mauris commodo a odio sit amet eleifend. Curabitur nunc orci, elementum at lectus vitae, egestas rutrum ante.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo mauris, hendrerit at felis in, feugiat blandit tortor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse posuere, lectus et pulvinar facilisis, felis mi faucibus purus, vel tempus purus dolor et enim. Aliquam sollicitudin velit ac enim pretium fermentum. Duis ac metus nec mauris varius volutpat vel sed lorem. Curabitur malesuada vehicula lorem at convallis. Nulla at augue dui. Vestibulum risus metus, scelerisque ut felis vel, sodales pretium libero. Proin efficitur lorem vitae diam facilisis vestibulum. Nam molestie suscipit nibh ac porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas bibendum, libero vel volutpat tincidunt, lacus lorem porttitor nisi, eu mollis turpis est vitae enim. Suspendisse potenti. Integer lacinia neque sit amet lorem rutrum, ac commodo mi porta. Mauris commodo a odio sit amet eleifend. Curabitur nunc orci, elementum at lectus vitae, egestas rutrum ante.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo mauris, hendrerit at felis in, feugiat blandit tortor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse posuere, lectus et pulvinar facilisis, felis mi faucibus purus, vel tempus purus dolor et enim. Aliquam sollicitudin velit ac enim pretium fermentum. Duis ac metus nec mauris varius volutpat vel sed lorem. Curabitur malesuada vehicula lorem at convallis. Nulla at augue dui. Vestibulum risus metus, scelerisque ut felis vel, sodales pretium libero. Proin efficitur lorem vitae diam facilisis vestibulum. Nam molestie suscipit nibh ac porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas bibendum, libero vel volutpat tincidunt, lacus lorem porttitor nisi, eu mollis turpis est vitae enim. Suspendisse potenti. Integer lacinia neque sit amet lorem rutrum, ac commodo mi porta. Mauris commodo a odio sit amet eleifend. Curabitur nunc orci, elementum at lectus vitae, egestas rutrum ante.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc justo mauris, hendrerit at felis in, feugiat blandit tortor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse posuere, lectus et pulvinar facilisis, felis mi faucibus purus, vel tempus purus dolor et enim. Aliquam sollicitudin velit ac enim pretium fermentum. Duis ac metus nec mauris varius volutpat vel sed lorem. Curabitur malesuada vehicula lorem at convallis. Nulla at augue dui. Vestibulum risus metus, scelerisque ut felis vel, sodales pretium libero. Proin efficitur lorem vitae diam facilisis vestibulum. Nam molestie suscipit nibh ac porttitor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas bibendum, libero vel volutpat tincidunt, lacus lorem porttitor nisi, eu mollis turpis est vitae enim. Suspendisse potenti. Integer lacinia neque sit amet lorem rutrum, ac commodo mi porta. Mauris commodo a odio sit amet eleifend. Curabitur nunc orci, elementum at lectus vitae, egestas rutrum ante.</p>
            </div>
            <div style="width: 40%;padding-top: 30px;">
            <h5>Test ArcAds</h5>
            <div id="div-id-6"></div>
            
            <hr>
            <h5>Test ArcAds</h5>
            <div id="div-id-7"></div>
            </div>
        </div>
        <script async src="/functions/cmp/cmp_loader.js" type="text/javascript"></script>
        </body>
        </html
    

### Type of Ads
- ### Ad with content defined inside a web site
    ```js
        pbjsInstance.registerAd({
            id: 'div-id-6',
            type: IMAGE_AD_TYPE,
            slotName: 'Businesscasestudies/Businesscasestudies_top_sidebar',
            adType: 'cube',
            display: 'desktop',
            dimensions: [
                [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
                [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
                [[1, 1],[320, 100],[320, 50], [300, 600],[300, 250],[160, 600],[120, 600]]
            ],
            iframeBidders: ['appnexus'],
            sizemap: {
                breakpoints: [ [1280, 0], [800, 0], [0, 0]],
                refresh: 'true'
            },
            bidding: {
                prebid: {
                    code: '/42150330/Businesscasestudies/Businesscasestudies_top_sidebar',
                    enabled: true,
                    bids: [{
                        bidder: 'appnexus',
                        labels: ['desktop', 'tablet', 'phone'],
                        params: {
                            placementId: '19366244'
                        }
                    }]
                }
            }
        });
    ```
    
- ### Ad inside of a text paragraphs
    ```js
        pbjsInstance.registerAd({
        type: BETWEEN_TEXT_AD_TYPE, //REQ
        style: { //REQ
            display: 'block'
        },
        //contentIdParagraphs: 'contentText', //REQ
        contentClassParagraphs: 'td-page-content', //REQ
        paragraphSplitTag: 'p',
        afterParagraph: 1, //REQ
        //PARAMS ASCEADS
        id: 'in-line-ad1',
        slotName: 'businessonline/businessonline_infeed_1',
        adType: 'cube',
        display: 'desktop',
        dimensions: [
            [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
            [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
            [[1, 1],[320, 100],[320, 50], [300, 600],[300, 250],[160, 600],[120, 600]]
        ],
        iframeBidders: ['appnexus'],
        sizemap: {
            breakpoints: [ [1280, 0], [800, 0], [0, 0]],
            refresh: 'true'
        },
        bidding: {
            prebid: {
                code: '/42150330/businessonline/businessonline_infeed_1',
                enabled: true,
                bids: [{
                    bidder: 'appnexus',
                    labels: ['desktop', 'tablet', 'phone'],
                    params: {
                        placementId: '19366244'
                    }
                }]
            }
        }
    });
    ```