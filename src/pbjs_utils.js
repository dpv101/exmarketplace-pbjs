import * as GLOBAL from './functions/globals';
import SlotImage from './functions/slot_image';
import SlotVideo from './functions/slot_video';
import Utils from './functions/utils';

//This class 
import FloorAd from './functions/types_ads/floor_ad';
import ParagraphAd from './functions/types_ads/paragraph_ad';

// View documentation: https://github.com/prebid/Prebid.js?files=1
import prebid from 'prebid.js';
import 'prebid.js/modules/appnexusBidAdapter';
import 'prebid.js/modules/prebidServerBidAdapter';
import 'prebid.js/modules/dfpAdServerVideo';
import 'prebid.js/modules/consentManagement';

//View documentation: https://github.com/washingtonpost/ArcAds
import {ArcAds as AsceAds} from 'arcads';

/**
* @desc This class is the principal class that initialize all requered for
* @desc prebid.js, ArcAds and googletag working correctly.
* @desc After initialize this class, always should called initAsceAds(config) method.
* @desc This method initialize ArcAds and the config structure can be found in: https://github.com/washingtonpost/ArcAds.
* @desc If the configuration has S2SConfig, should called setS2sConfig(object)
* @desc method with de configuration of s2sConfig. Doc: http://prebid.org/dev-docs/get-started-with-prebid-server.html
* @desc After you can add Ads to this class through of method registerAd(adUnit).
* @desc Finally you should call the method loadAds() for initialize the auction and render de ads.
**/
export default class PbjsUtils {
    constructor() {
        this.PREBID_TIMEOUT = GLOBAL.PREBID_TIMEOUT;
        this.FAILSAFE_TIMEOUT = GLOBAL.FAILSAFE_TIMEOUT;
        this.DEBUG = GLOBAL.DEBUG;
        
        this.AD_UNITS_IMAGE = [];
        this.AD_UNITS_VIDEO = [];
        
        
        this.slotImageArray = [];
        this.slotVideoArray = [];
        
        this.cmpCheck = false;
        this.cpmData = null;

        this.asceAds = null;
        this.s2sConfig = null;
        this.priceGranularity = 'dense';
        
        global.pbjs = prebid;
        this.PBJS = prebid;

        if(typeof(googletag) != 'undefined') {
            this.GOOGLETAG = googletag;
        } else {
            this.GOOGLETAG = {};
        }

        if(this.DEBUG)
            global.pbjsUtils = this;
    }

    /**
    * @desc This Function initialize a instance from ArcAds. Doc: https://github.com/washingtonpost/ArcAds
    * @param {config} object - View documentation in https://github.com/washingtonpost/ArcAds
    **/
    initAsceAds(config) {
        this.asceAds = new AsceAds(
            config, 
            (event) => {
                console.log('Advertisement has loaded...', event)
        });

        global.asceAds = this;
    }
    
    /**
    * @desc Assign S2sConfig of prebid.js. Doc: http://prebid.org/dev-docs/get-started-with-prebid-server.html
    * @param {config} object - View documentation in https://github.com/washingtonpost/ArcAds
    **/
    setS2sConfig(config){
        this.s2sConfig = config;
    }

    /**
    * @desc Assign S2sConfig of prebid.js to this instance. Doc: http://prebid.org/dev-docs/get-started-with-prebid-server.html
    * @param {config} object - View documentation in https://github.com/washingtonpost/ArcAds
    **/
    setPriceGranularity(value) {
        this.priceGranularity = value;
    }

    /**
    * @desc Get Ad units from slotImage class. The code is equal to code of adUnit prebid.js object
    * @return {codes} Array - View documentation in https://github.com/washingtonpost/ArcAds
    **/
    getAdUnitsCodes() {
        return this.slotImageArray.map((slotImage) => slotImage.getCode())
                    .concat(this.slotVideoArray.map((slotVideo) => slotVideo.getCode()))
    }
    
    /**
    * @desc This function generates the structure to prebid.js config
    * @return {config} object - View documentation in http://prebid.org/dev-docs/publisher-api-reference.html
    **/
    generateConfigPbjs() {
        let config = {
            debug: this.DEBUG,
            publisherDomain: "https://businesscasestudies.co.uk",
            priceGranularity: this.priceGranularity,
            enableSendAllBids: false,
            targetingControls: {
                alwaysIncludeDeals: true
            },
            cache: {
                url: 'https://prebid.adnxs.com/pbc/v1/cache'
            },
            consentManagement: {
                gdpr: {
                    consentData: this.cpmData
                }
            }
        }

        if(this.s2sConfig) config.s2sConfig = this.s2sConfig;
        return config;
    }

    /**
    * @desc This function register Ad in this instance.
    * @param {adUnit} object - The structure of adUnit 
    **/
    registerAd(adUnit) {
        let adTypeInstance = null;
        switch(adUnit.type) {
            case GLOBAL.FLOOR_AD_TYPE:
                adTypeInstance = new FloorAd(adUnit);
                break;
            case GLOBAL.BETWEEN_TEXT_AD_TYPE:
                adTypeInstance = new ParagraphAd(adUnit);
                break;
        }

        if(adTypeInstance) adTypeInstance.processSlot();

        this.slotImageArray.push(new SlotImage(adUnit.id, adUnit));
        this.AD_UNITS_IMAGE.push(adUnit);
    }

    /**
    * @desc Initialize prebid.js through ArcAds after of initialize and accept cmp.
    **/
    loadAds() {
        if(typeof(window.__cmp) == 'undefined') {
            if(this.DEBUG) console.log("CPM NOT LOAD YET");
            setTimeout(() => {this.loadAds()}, 200);
            return;
        }

        Utils.cmpCheck(this);
        if(!this.cmpCheck || this.asceAds == null) {
            if(this.DEBUG) console.log("CPM NOT CHECK OR ASCEADS NOT LOAD YET");
            setTimeout(() => {this.loadAds()}, 200);
            return;
        }

        this.PBJS.setConfig(this.generateConfigPbjs());

        if(this.slotImageArray.length > 0) {
            this.asceAds.registerAdCollection(this.AD_UNITS_IMAGE);
        }
        
        this.PBJS.processQueue();
        setTimeout(() => {this.showAddsInContent();}, this.FAILSAFE_TIMEOUT);
    }

    /**
    * @desc Sometimes prebid.js not render ads that won the auction, 
    * @desc this function force to render ads that won the auction but not was rendered.
    **/
    showAddsInContent() {
        let count = 0;
        this.PBJS.getAllPrebidWinningBids().forEach((x) => {
            let slotImage = Utils.getSlotImageByCode(x.adUnitCode, this.slotImageArray);
            let div = document.getElementById(slotImage.idContent);
            if(div) {
                let size = x.size.split('x');
                let nameIframe = slotImage.getCode() + '_' + count;
                div.style.width = size[0] + 'px';
                div.style.height = size[1] + 'px';
                div.childNodes[0].style.width = div.style.width;
                div.childNodes[0].style.height = div.style.height;
                div.childNodes[0].innerHTML = '<iframe id="google_ads_iframe_' + nameIframe + '" title="3rd party ad content" name="google_ads_iframe_' + nameIframe + '" width="300" height="600" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" srcdoc="" data-google-container-id="1" style="border: 0px; vertical-align: bottom;" data-load-complete="true"></iframe>';
                pbjs.renderAd(div.childNodes[0].childNodes[0].contentWindow.document, x.adId);
                count++;
            } else {
                if(this.DEBUG) console.log(slotImage.idContent + "---->" + nameIframe + " NOT FOUND INTO DOM HTML");
            }
        });
        if(count > 0) {
            googletag.cmd.push(() => {
                googletag.pubads().refresh();
            });
        }
    }
}