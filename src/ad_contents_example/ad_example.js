/**
 ************************
 ************************
 ************************
 * @desc The scripts that you want to add to the website must be configured on this file 
 ************************
 ************************
 ************************
**/

import PbjsUtils from '../pbjs_utils.js'
import {IMAGE_AD_TYPE, VIDEOJS_INSTREAM, VIDEO_AD_TYPE, FLOOR_AD_TYPE, BETWEEN_TEXT_AD_TYPE} from '../functions/globals';

let pbjsInstance = new PbjsUtils();
pbjsInstance.setPriceGranularity('low');
pbjsInstance.setS2sConfig({
    accountId: '1',
    enabled: true,
    defaultVendor: 'appnexus',
    bidders: ['appnexus'],
    publisherDomain: "http://businesscasestudies.co.uk", // Used for SafeFrame creative.
    timeout: 1500,
    adapter: 'prebidServer',
    endpoint: 'https://hbserver.exmarketplace.com/openrtb2/auction',
    syncEndpoint: 'https://hbserver.exmarketplace.com/cookie_sync',
});


pbjsInstance.initAsceAds({
    dfp: {
        id: '42150330'
    },
    bidding: {
        prebid: {
        enabled: true,
        timeout: 2000,
        sizeConfig: [
            {
            'mediaQuery': '(min-width: 1024px)',
            'sizesSupported': [[970, 250], [970, 90], [728, 90], [468, 60], [300, 600], [300, 250], [160, 600], [120, 600]],
            'labels': ['desktop']
            }, 
            {
            'mediaQuery': '(min-width: 480px) and (max-width: 1023px)',
            'sizesSupported': [[728, 90], [468, 60], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 250]],
            'labels': ['tablet']
            }, 
            {
            'mediaQuery': '(min-width: 0px)',
            'sizesSupported': [[360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 250]],
            'labels': ['phone']
            }
        ]
        }
    }
});

pbjsInstance.registerAd({
    id: 'div-id-6',
    type: IMAGE_AD_TYPE,
    slotName: 'Businesscasestudies/Businesscasestudies_top_sidebar',
    adType: 'cube',
    display: 'desktop',
    dimensions: [
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1],[320, 100],[320, 50], [300, 600],[300, 250],[160, 600],[120, 600]]
    ],
    iframeBidders: ['appnexus'],
    sizemap: {
        breakpoints: [ [1280, 0], [800, 0], [0, 0]],
        refresh: 'true'
    },
    bidding: {
        prebid: {
            code: '/42150330/Businesscasestudies/Businesscasestudies_top_sidebar',
            enabled: true,
            bids: [{
                bidder: 'appnexus',
                labels: ['desktop', 'tablet', 'phone'],
                params: {
                    placementId: '19366244'
                }
            }]
        }
    }
});

pbjsInstance.registerAd({
    id: 'div-id-7',
    type: IMAGE_AD_TYPE,
    slotName: 'Businesscasestudies/Businesscasestudies_bottom_sidebar',
    adType: 'cube',
    display: 'desktop',
    dimensions: [
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1],[320, 100],[320, 50], [300, 600],[300, 250],[160, 600],[120, 600]]
    ],
    iframeBidders: ['appnexus'],
    sizemap: {
        breakpoints: [ [1280, 0], [800, 0], [0, 0]],
        refresh: 'true'
    },
    bidding: {
        prebid: {
            code: '/42150330/Businesscasestudies/Businesscasestudies_bottom_sidebar',
            enabled: true,
            bids: [{
                bidder: 'appnexus',
                labels: ['desktop', 'tablet', 'phone'],
                params: {
                    placementId: '19366244'
                }
            }]
        }
    }
});

pbjsInstance.registerAd({
    type: BETWEEN_TEXT_AD_TYPE, //REQ
    style: { //REQ
        display: 'block'
    },
    //contentIdParagraphs: 'contentText', //REQ
    contentClassParagraphs: 'td-page-content', //REQ
    paragraphSplitTag: 'p',
    afterParagraph: 1, //REQ
    //PARAMS ASCEADS
    id: 'in-line-ad1',
    slotName: 'businessonline/businessonline_infeed_1',
    adType: 'cube',
    display: 'desktop',
    dimensions: [
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1],[320, 100],[320, 50], [300, 600],[300, 250],[160, 600],[120, 600]]
    ],
    iframeBidders: ['appnexus'],
    sizemap: {
        breakpoints: [ [1280, 0], [800, 0], [0, 0]],
        refresh: 'true'
    },
    bidding: {
        prebid: {
            code: '/42150330/businessonline/businessonline_infeed_1',
            enabled: true,
            bids: [{
                bidder: 'appnexus',
                labels: ['desktop', 'tablet', 'phone'],
                params: {
                    placementId: '19366244'
                }
            }]
        }
    }
});

pbjsInstance.registerAd({
    type: BETWEEN_TEXT_AD_TYPE, //REQ
    style: { //REQ
        display: 'block'
    },
    //contentIdParagraphs: 'contentText', //REQ
    contentClassParagraphs: 'td-page-content', //REQ
    paragraphSplitTag: 'p',
    afterParagraph: 2, //REQ
    //PARAMS ASCEADS
    id: 'in-line-ad2',
    slotName: 'businessonline/businessonline_infeed_2',
    adType: 'cube',
    display: 'desktop',
    dimensions: [
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1], [360, 300], [360, 100], [360, 50], [336, 280], [320, 100], [320, 50], [300, 600], [300, 250], [160, 600], [120, 600]],
        [[1, 1],[320, 100],[320, 50], [300, 600],[300, 250],[160, 600],[120, 600]]
    ],
    iframeBidders: ['appnexus'],
    sizemap: {
        breakpoints: [ [1280, 0], [800, 0], [0, 0]],
        refresh: 'true'
    },
    bidding: {
        prebid: {
            code: '/42150330/businessonline/businessonline_infeed_2',
            enabled: true,
            bids: [{
                bidder: 'appnexus',
                labels: ['desktop', 'tablet', 'phone'],
                params: {
                    placementId: '19366244'
                }
            }]
        }
    }
});

pbjsInstance.registerAd({
    type: FLOOR_AD_TYPE, //REQ
    style: { //REQ
        display: 'block',
        position: 'fixed',
        bottom: '340px',
        width: '98%',
        left: '50%',
        'background-color': '#FFFFFF',
        transform: 'translateX(-050%)',
        'border': '1px solid #eee',
    },
    closeButton: true, //REQ
    styleCloseButton: { //REQ
        position: 'absolute',
        right: '5px',
        top: '5px',
        'border-radius': '25px',
        border: '1px solid #eee',
        'background-color': 'transparent'
    },
    //timeOutClose: 30000, //REQ
    //PARAMS ASCEADS
    id: 'floor-ad',  //Equale To ID in DIV
    adType: 'cube',
    display: 'desktop',
    iframeBidders: ['appnexus'],
    slotName: 'businessonline/businessonline_floorad',  //Without Company ID
    dimensions: [ [[970, 90], [728, 90], [468, 60], [414, 100], [414, 50]], 
                  [[970, 90], [728, 90], [468, 60], [414, 100], [414, 50], [360, 100], [360, 50], [320, 100]], 
                  [[360, 100], [360, 50], [320, 100], [320, 50]] ],
    sizemap: {
        breakpoints: [ [1800, 0], [800, 0], [0, 0] ],
        refresh: true
    },
    enabled: true,
    display: 'all',
    bidding: {
        prebid: {
            code: '/42150330/businessonline/businessonline_floorad',
            enabled: true,
            bids: [{
                bidder: 'appnexus',
                labels: ['desktop', 'tablet', 'phone'],
                params: {
                    placementId: '19366244'
                }
            }]
        }
    }
});

pbjsInstance.loadAds();