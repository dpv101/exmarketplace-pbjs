/**
* @desc This class is called from the principal class when the ad is the type image.
**/
export default class SlotImage {
    constructor(idContent, adUnit) {
        this.idContent = idContent;
        this.adUnit = adUnit;
    }

    getCode() {
        return this.adUnit.bidding.prebid.code;
    }

    getPrebidUnit() {
        return this.adUnit.bidding.prebid;
    }

    getSizes() {
        return this.adUnit.bidding.prebid.mediaTypes.banner.sizes;
    }

    getId() {
        return this.adUnit.id;
    }
} 