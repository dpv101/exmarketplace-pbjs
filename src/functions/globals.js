/**
 ******************
 ******************
 ******************
 ******************
 ******************
 * @desc THIS FILE CONTAINS ALL GLOBAL VARS.
 ******************
 ******************
 ******************
 ******************
 ******************
**/

export const URL_VIDEO_JS_VAST_VPAID_SCRIPT = 'https://cdnjs.cloudflare.com/ajax/libs/videojs-vast-vpaid/2.0.2/videojs_5.vast.vpaid.min.js';

export const PREBID_TIMEOUT = 1500;
export const FAILSAFE_TIMEOUT = 3000;
export const DEBUG = true;

//TYPES OF AD CONTENTS
export const IMAGE_AD_TYPE = 'Image';
export const VIDEO_AD_TYPE = 'Video';
export const BETWEEN_TEXT_AD_TYPE = 'BetweenText';
export const FLOOR_AD_TYPE = 'Floor';

//TYPES OF VIDEO INSTREAM
export const VIDEOJS_INSTREAM = "VideoJS"
export const BRIGHTCOVE_INSTREAM = "Brightcove"