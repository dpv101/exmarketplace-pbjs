/**
 ************************
 ************************
 ************************
 * @desc THIS FILE IS ONLY TO TEST DE ADS IN A LOCAL ENVIRONMENT
 * @desc This script add CMP to test page index.html
 ************************
 ************************
 ************************
**/

var elem = document.createElement("script"),
    cmpJs = "cmp.js",
    urlParams = window.location.search;
urlParams.replace(/([?&])forceEU=(true|false)(&|$)/g, function(e, n, a) {
    return cmpJs = "true" === a ? "cmp-inEU.js" : "cmp-notEU.js", n
}), elem.src = "https://static.quantcast.mgr.consensu.org/v34/" + cmpJs, elem.async = !0, elem.integrity = "sha256-wcK00ailZ7l1Ms9uqdH9tPXyZOiUXgWY28cCd+HzMM4= sha256-rppMsoAp/s+PmlDSPRncURhvFIgHaFKMhUGAdhXTQyc= sha256-qg1LZBCXUrTUo2Dr0qGpsmdjZN6AaxPnYQrcbRn7ghk= sha256-ot3T4uPdxNNqyiVJ8a35qyxwRRvDQGHFcwtbIkAAbUo=", elem.crossOrigin = "anonymous", elem.type = "text/javascript";
var scpt = document.getElementsByTagName("script")[0];
scpt.parentNode.insertBefore(elem, scpt);
var config = {};
urlParams.replace(new RegExp("([^?=&]+)=([^&]*)?", "g"), function(e, n, a) {
        try {
            var c = decodeURIComponent(n.replace(/\+/g, " ")),
                t = a ? decodeURIComponent(a.replace(/\+/g, " ")) : null;
            config[c] = JSON.parse(t)
        } catch (e) {
            console.error("ignoring url config parameter", e)
        }
    }), config["Display UI"] || (config["Display UI"] = "always"), config["Publisher Name"] || (config["Publisher Name"] = "Test Demo"), config["Publisher Logo"] || (config["Publisher Logo"] = "https://www.quantcast.com/wp-content/uploads/2018/03/quantcast-logo-1200.png"), config["Publisher Purpose IDs"] || (config["Publisher Purpose IDs"] = [1, 3, 4]),
    function() {
        function e(e) {
            var n, a = "string" == typeof e.data;
            if ((n = a ? -1 != e.data.indexOf("__cmpCall") ? JSON.parse(e.data) : {} : e.data).__cmpCall) {
                var c = n.__cmpCall;
                window.__cmp(c.command, c.parameter, function(n, t) {
                    var o = {
                        __cmpReturn: {
                            returnValue: n,
                            success: t,
                            callId: c.callId
                        }
                    };
                    e.source.postMessage(a ? JSON.stringify(o) : o, "*")
                })
            }
        }! function e() {
            if (!window.frames.__cmpLocator)
                if (document.body) {
                    var n = document.body,
                        a = document.createElement("iframe");
                    a.style = "display:none", a.name = "__cmpLocator", n.appendChild(a)
                } else setTimeout(e, 5)
        }(), window.__cmp = function(e) {
            var n = arguments;
            if (!n.length) return __cmp.a;
            if ("ping" === n[0]) n[2]({
                gdprAppliesGlobally: !0,
                cmpLoaded: !1
            }, !0);
            else {
                if ("__cmp" == e) return !1;
                void 0 === __cmp.a && (__cmp.a = []), __cmp.a.push([].slice.apply(n))
            }
        }, window.__cmp.gdprAppliesGlobally = !0, window.__cmp.msgHandler = e, window.addEventListener ? window.addEventListener("message", e, !1) : window.attachEvent("onmessage", e)
    }(), window.__cmp("init", config);