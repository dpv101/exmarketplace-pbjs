import videojs from 'video.js';
import axios from 'axios';

/**
* @desc This class only contains static functions because 
* @desc all functions are for small processing of information 
* @desc such as changes in structure and among others
**/
export default class Utils{

    /**
     * @desc Function that allows get a code(Ej. script url) and call a callback function
     * @param {url} string - Url of code to get.
     * @param {callback} string - Function to call after to get code from url
    */
    static getScript(url, callback) {
        axios.get(url).then((response) => {
            callback(response.data);
        });
    }

    /**
     * @desc Function that allows get a code(Ej. script url) and insert within head of document
     * @param {url} string - Url of code to get.
     * @param {options} object - {async: false, defer: false}
     * @param {callback} string - Function to call after to get code from url
     * @param {params} object - All params to pass to callback function
    */
    static importScriptHead(url, options, callback, params) {
        const tag = document.createElement('script');
        tag.src = url;
        tag.async = options.async || false;
        tag.defer = options.defer || false;
        (document.head || document.documentElement).appendChild(tag);
        if (callback) {
            callback(params);
        }
    }

    /**
     * @desc Function that allows insert code within specific DOM element
     * @param {idDom} string - ID from DOM element to insert code
     * @param {script} string - Code to insert
    */
    static importScriptIntoDom(idDom, script) {
        const tag = document.createElement('script');
        tag.innerHTML = script;
        document.getElementById(idDom).appendChild(tag);
    }


    /**
     * @desc This load all necesary to render Video with Brightcove. Doc: http://prebid.org/examples/video/instream/brightcove/pb-ve-brightcove.html
     * @param {idContent} string - Reference to object where add video.
     * @param {options} object - Object with options of Brightcove
    */
    static invokeVideoBrightcove(idContent, options) {
        bc(idContent).ima3(options);
        videojs(idContent).ready(function() {
            var myPlayer = this;
            myPlayer.catalog.getVideo("5530036758001", function(error, video){
               myPlayer.catalog.load(video);
               if(error) {
                     console.log("There was an error retrieving the media file: " + error);
               }
             });

             myPlayer.on("ima3error", function() {
               console.log("There was an ima3 error.");
             });
       });
    }

    /**
     * @desc This function validates if cmp was checked
     * @param {refToBackAnswer} object - Reference to object where save if cmp was check
    */
    static cmpCheck(refToBackAnswer) {
        window.__cmp('getVendorConsents', null, function(vendorConsents) {
            window.CookieIfAccepted = vendorConsents.metadata;
            if (window.CookieIfAccepted === null || window.CookieIfAccepted === 'undefined') {
                refToBackAnswer.cmpCheck = false;
            } else if(window.CookieIfAccepted.length > 0){
                refToBackAnswer.cmpCheck = true;
            }
            refToBackAnswer.cpmData = vendorConsents;
        });
    }

    /**
     * @desc This function return instance slotImage type where code from slotImage equal to code param
     * @param {code} string - Code to search
     * @param {slotImageArray} array - Array with objects of slogImage type
     * @return {SlotImage} - SlotImage type object
    */
    static getSlotImageByCode(code, slotImageArray) {
        for(x in slotImageArray){
            if(slotImageArray[x].getId() == code || slotImageArray[x].getCode() == code) return slotImageArray[x];
        }
    }

    /**
     * @desc This function convert a object with css keys and values in a string css code
     * @param {style} Object - Style object structure
     * @return {styleString} - Return the style css structure
    */
    static getStyleString(style) {
        return Object.entries(style).map(([k, v]) => `${k}:${v}`).join(';');
          
    }

    /**
     * @desc This function creates a div within a content and apply style object
     * @param {id} string - ID of DomObject to create
     * @param {targetId} Object - ID of DomObject where will create the div
     * @param {target} DOMobject - DomObject where will create the div
     * @param {target} DOMobject - Style object with css keys and values
     * @return {div} - DomObject created
    */
    static createContentDiv({id, targetId, target, style}) {
        if(targetId) {
            target = document.getElementById(targetId);
        } else if(!target){
            target = document.body;
        }
        if(target) {
            let div = document.createElement('div');
            div.id = id;
            div.setAttribute('style', Utils.getStyleString(style));
            target.append(div);
            return div;
        }
    }

    /**
     * @desc This function get a DomObject from a content. Ej. Get second tag 'p' from a content <div id="example"></div> 
     * @desc --> getTagFromContent(document.getElementById("example"), 'p', 1)
     * @param {content} DomObject - DomObject from to get de tag idx
     * @param {tag} string - String tag name to find within a content
     * @param {idx} integer - Index of tag to get
     * @return {tagDomObject} - DomObject found.
    */
    static getTagFromContent(content, tag, idx) {
        try{
            return content.getElementsByTagName(tag)[idx];
        } catch {
            console.log("Tag: " + tag + " idx " + idx + " NOT FOUND");
        }
    }
}