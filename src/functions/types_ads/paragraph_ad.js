import Utils from '../utils';

/**
* @desc This class contains all events required for de ParagraphAd.
* @desc If the principal class PbjsUtils get a Ad of type: BETWEEN_TEXT_AD_TYPE
* @desc inside of method registerAd will create a instance of this type of class.
* @desc Paragraph ads are ads that appear after a paragraph configured through 
* @desc the ads options. This class is responsible for identifying the paragraph 
* @desc and putting it right after.
**/
export default class ParagraphAd {
    constructor(adUnit) {
        this.options = adUnit;
        this.contentAd = null;
    }

    /**
    * @desc This function create a content for ParagraphAd and generate all contents according
    * @desc to options of Ad.
    **/
    processSlot() {
        if(this.options.afterParagraph) {
            let content = this.getContent();
            this.contentAd = Utils.createContentDiv({target: content, ...this.options});
        }
    }

    /**
    * @desc This function get a content for ParagraphAd according to options
    * @return {DomObject} - DomObject with content where will create the ad.
    **/
    getContent() {
        if(this.options.contentClassParagraphs) {
            return Utils.getTagFromContent(
                document.getElementsByClassName(this.options.contentClassParagraphs)[0], 
                this.options.paragraphSplitTag,
                this.options.afterParagraph - 1);
        } else {
            return Utils.getTagFromContent(
                document.getElementById(this.options.contentIdParagraphs), 
                this.options.paragraphSplitTag,
                this.options.afterParagraph - 1);
        }
    }

}