import Utils from '../utils';

/**
* @desc This class contains all events required for de FloorAds.
* @desc If the principal class PbjsUtils get a Ad of type: FLOOR_AD_TYPE
* @desc inside of method registerAd will create a instance of this type of class.
* @desc If the add has a options how showWhenScroll this class will create
* @desc the triggers to meet the parameters.
**/
export default class FloorAd {
    constructor(adUnit) {
        this.options = adUnit;
        this.contentAd = null;
    }

    /**
    * @desc This function create a content for FloorAd and generate all events according
    * @desc to options of Ad.
    **/
    processSlot() {
        let style = {
            margin: 'auto'
        };

        let content = Utils.createContentDiv({
            targetId: null, 
            id: 'content-' + this.options.id,
            style: this.options.style
        })
        this.contentAd = Utils.createContentDiv({target: content, id: this.options.id, style});

        if(this.options.closeButton) {
            let button = document.createElement('button');
            button.style = Utils.getStyleString(this.options.styleCloseButton);
            button.innerHTML = 'X';
            content.append(button);
            button.onclick = (e) => {
                content.style.display = 'none';
            };
        }

        if(this.options.timeOutClose) {
            setTimeout(() => {
                content.style.display = 'none';
            }, this.options.timeOutClose);
        }

        if(this.options.showWhenScroll){
            this.addTriggerWheScroll();
        }
    }

    /**
    * @desc This function create a trigger when the FloorAd has showWhenScroll option
    **/
    addTriggerWheScroll(){
        if(this.contentAd && this.options.showWhenScroll) {
            document.body.onscroll = () => {
                if(document.body.scrollTop > this.options.showWhenScroll || document.documentElement.scrollTop > this.options.showWhenScroll){
                    this.contentAd.style.display = 'block';
                } else {
                    this.contentAd.style.display = 'none';
                }
            };
        }
    }

}