/**
* @desc This class is called from the principal class when the ad is the type video.
**/
export default class SlotVideo {
    constructor(idContent, instream, adUnit, buildVideoUrlParams) {
        this.idContent = idContent;
        this.instream = instream;
        this.adUnit = adUnit;
        this.buildVideoUrlParams = buildVideoUrlParams;
    }

    getCode() {
        return this.adUnit.code;
    }

    getSizes() {
        return this.adUnit.mediaTypes.video.playerSize;
    }

    getOptionsVideoPlayer() {
        return this.buildVideoUrlParams.optionsVideoPlayer;
    }

    getVideoUrl() {
        return this.buildVideoUrlParams.optionsVideoPlayer.vpaidFlashLoaderPath;
    }

    getParamsPrebid() {
        return this.buildVideoUrlParams.paramsPrebid;
    }
} 