module.exports = {
    plugins: [
      "transform-object-assign", // required (for IE support) and "babel-plugin-transform-object-assign" 
                                // must be installed as part of your package.
      require('prebid.js/plugins/pbjsGlobals.js') // required!
    ],
    presets: [
      [
        '@babel/preset-env',
        {
          targets: {
            node: 'current',
          },
        },
      ],
    ],
  };